/*
 *
 * San Diego State University: Computer Science 570 - Assignment01
 *
 * This program is written using the C programming language.
 * A spellchecker is required to test if the dictionary tree
 * within the dictionary.c class is working as intended.
 * This spellchecker will pass three separate arguments
 * (two of which are text files), check for invalid
 * number of arguments being passed, allocate
 * memory for the words being copied from the files,
 * check for specific delimiters within the lines being read,
 * and finally call the insert & find functions
 * to build the dictionary tree and print the misspelled words
 * within the first text document passed.
 *
 * @Author: Roman Mattia
 * @Date:   2020-01-30
 * @RedID:  820771491
 * @File:   spellcheck.c
 *
 */

#include "dictionary.h"

int main(int argc, char **argv) {
    
    const char *delimiters = "\n\r !\"#$%&()*+,-./0123456789:;<=>?@[\\]^_`{|}~";
    char buffer[256];

    /*
     * Checks for valid number of arguments being passed.
     */
    if(argc != 3) {
        exit(-1);
    }

    struct dictentry *root = getNewNode();
    
    FILE *fp = fopen(argv[2], "r"); //opens the second text file passed (the spell list)
    while(fgets(buffer, sizeof(buffer), fp)) {
        char *linecopy = (char *)malloc(sizeof(buffer));
        strcpy(linecopy, buffer);
        char *wordArr = (char*)strtok(linecopy, delimiters);

        while(wordArr != NULL) {
            insert(root, wordArr);
            wordArr = strtok(NULL, delimiters);
        }

        free(linecopy);
    }

    FILE *passedTextFile = fopen(argv[1], "r"); //opens the first text file passed (the document to be checked)
    while(fgets(buffer, sizeof(buffer), passedTextFile)) {
        char *linecopy = (char *)malloc(sizeof(buffer));
        strcpy(linecopy, buffer);
        char *wordArr = (char *)strtok(linecopy, delimiters);

        while(wordArr != NULL) {
            find(root, wordArr);
            wordArr = strtok(NULL, delimiters);
        }

        free(linecopy);
    }

    return 0;
}

