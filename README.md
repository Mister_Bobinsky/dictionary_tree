# ~ San Diego State University ~ Computer Science 570: Operating Systems

## Assignment01 - Dictionary Tree in C: Description

A dictionary tree has been implemented that allows the caller to pass a spell list of accepted words and an additional text document that will check if those accepted words are within the passed text document.

### Manifest
* `dictionary.h`
    * The dictionary.h file is a header file used to enforce the function definitions in the dictionary.c file. It is also used to create the dictentry, initialize the isEndOfWord boolean, and finally set the the *next array to the 27 different characters being checked for in the search function => [a - z] & single quote [']. This header file also creates the enum's returned by the ResultType return type.

* `dictionary.c`
    * A dictionary tree has been implemented that allows the caller to pass a spell list of accepted words and an additional text document that will check if those accepted words are within the passed text document.

* `spellcheck.c`
    * A spellchecker is required to test if the dictionary tree within the dictionary.c class is working as intended. This spellchecker will pass three separate arguments (two of which are text files), check for invalid number of arguments being passed, allocate memory for the words being copied from the files, check for specific delimiters within the lines being read, and finally call the insert & find functions to build the dictionary tree and print the misspelled words within the first text document passed.

* `Makefile`
    * The Makefile allows the user to use the CLI for program manipulation. It allows access to the cleaning of the working directory which removes all output and executable files (make clean), and the building of the dictionary library & the spellcheck.c program (make).

* `USER_PASSED_TEST_DOC.TXT`
    * User passed test document that will check if any of the words in the text file are misspelled.

* `USER_PASSED_SPELL_LIST.TXT`
    * User passed spell list that populates the tree dictionary with the provided words.

### Usage
1. Run the program by entering the following command into the terminal:
    * `./dictionary USER_PASSED_TEST_DOC.TXT USER_PASSED_SPELL_LIST.TXT`
    
2. If you make changes, you can recompile the library and test program using:
    * `make`

3. To clean the directory once finished, enter the following command into the terminal:
    * `make clean`

### Implementation Diagram

![Example Dictionary Tree Diagram][example]

[example]: /Tree_Dictionary_Representation.PNG
