/*
 *
 * San Diego State University: Computer Science 570 - Assignment01
 *
 * This program is written using the C programming language.
 * A dictionary tree has been implemented that allows
 * the caller to pass a spell list of accepted words and
 * an additional text document that will check if those 
 * accepted words are within the passed text document.
 *
 * @Author: Roman Mattia
 * @Date:   2020-01-30
 * @RedID:  820771491
 * @File:   dictionary.c
 *
 */

#include "dictionary.h"

/*
 * dictentry *getNewNode() function
 * - This function allows for the allocation of new nodes
 *   within the dictionary tree. It is also used to set
 *   the starting isEndOfWord boolean check to false.
 */
dictentry *getNewNode() {
    struct dictentry *aNode = (dictentry *)malloc(sizeof(dictentry));
    aNode->isEndOfWord = false;
    
    int i;
    for(i = 0; i < Nchars; i++) {
        aNode->next[i] = NULL;
    }
    
    return aNode;
}

/*
 * int wordToChildNodeIndex(char letter) function
 * - This function is exetremely important since it
 *   provides two major uses to the dictionary tree:
 *   the first use being that when our spellcheck.c
 *   file passes over a file argument, and the words
 *   within those file arguments have been tokenized
 *   and passed to here it converts each individual character
 *   to lowercase THEN it assigns that character
 *   an index location using it's Ascii values.
 *   This index location allows our insert function
 *   to build the dictionary tree and assign indexes
 *   to each letter and it allows our search function 
 *   to find the paths to the words that were passed 
 *   in from the spell list.
 */
int wordToChildNodeIndex(char letter) {
    if(tolower(letter) >= 'a' && tolower(letter) <= 'z') { //checks [a - z]
        return tolower(letter) - 'a';
    } else if(tolower(letter) == '\'') { //check for single quote [']
        return 26;
    }
    
    return -1;
}

/*
 * ResultType insert(struct dictentry *dictnode, const char *characters)
 * - This fucntion (and the use of the wordToChildNodeIndex function
 *   will build our dictionary tree and create paths
 *   to the words passed in from the spell list.
 *
 * - The start of this function creates a for loop that
 *   runs until the end of each word (characters) passed in.
 *   The index of each individual character is found.
 *   
 * - The first if statement checks if the next childnode
 *   in the dictionary tree is NULL, if it is NULL
 *   then we know that we have a new character to insert
 *   into that parent node's children. Once the for loop reaches
 *   the end of the word (characters) we set that node's
 *   isEndOfWord to true and return ADDED as our ResultType.
 *   This step is paramount since without checking if
 *   isEndOfWord is true the find function would return
 *   every word as MISSPELLED since the end of a word
 *   would never be reached.
 *
 * - The else statement at the end, accounts for if the letter
 *   at that parent node's child has already been inserted.
 *   At this point all we need to do is make a reference
 *   to our dictnode (root) and move the dictnode to
 *   to it's next child node index location.
 */
ResultType insert(struct dictentry *dictnode, const char *characters) {
    int i;
    for(i = 0; i < strlen(characters); i++) {
        int childIndexLoc = wordToChildNodeIndex(characters[i]);

        if(dictnode->next[childIndexLoc] == NULL) {
            dictentry *root = dictnode;
            dictnode = getNewNode();
            root->next[childIndexLoc] = dictnode;

            if(i == strlen(characters) - 1) {
                dictnode->isEndOfWord = true;
                return ADDED;
            }
        } else {
            dictentry *root = dictnode;
            dictnode = dictnode->next[childIndexLoc];
        }
    }
}

/*
 * ResultType find(struct dictentry *dictnode, const char *targetword)
 * - This function is the backbone of the first file
 *   arguement passed in the spellcheck.c file.
 *   Very similarly to the insert function,
 *   the find function uses a for loop to iterate
 *   through the entire length of the targetword
 *   being searched for.
 *
 * - It will check the index location of each
 *   individual character from the targetword
 *   to the index location of each node in the tree
 *   beginning from root onwards.
 *
 * - If the index location of the targetword
 *   cannot be found within the node being searched
 *   then it's evident that that letter
 *   for whatever word that was being searched for
 *   isn't within the dictionary tree and can be
 *   returned back to the screen in its entirety 
 *   as a MISSPELLED word.
 *
 * - If the index location of the targetword
 *   is not NULL then a new reference
 *   can be created just like in the insert function
 *   to the dictnode, and the dictnode can be moved
 *   to the next node's index. This step comes with one
 *   extra caveat which is having to check if the end of the 
 *   targetword has been reached AND having to check if that
 *   last node has been flagged with isEndOfWord equal to false/true.
 *   If it's false, then the end of the word hasn't been reached
 *   and the word can be returned back to the screen in its entirety
 *   as a MISSPELLED word.
 *   If it's true then the word is within the dictionary tree,
 *   and the function can simply return that the word EXISTS.
 */
ResultType find(struct dictentry *dictnode, const char *targetword) {
    int i;
    for(i = 0; i < strlen(targetword); i++) {
        int isIndex = wordToChildNodeIndex(targetword[i]);
        if(isIndex == - 1) {
            printf("%s\n", targetword);
            return BADCHARS;
        } else if(dictnode->next[isIndex] == NULL) {
            printf("%s\n", targetword);
            return MISSPELLED;
        } else if(dictnode->next[isIndex] != NULL) {
            dictentry *root = dictnode;
            dictnode = dictnode->next[isIndex];
                if(i == strlen(targetword) - 1 && dictnode->isEndOfWord == false) {
                    printf("%s\n", targetword);
                    return MISSPELLED;
                }
        } else {
            return EXISTS;
        }
    }
}

