/*
 *
 * San Diego State University: Computer Science 570 - Assignment01
 *
 * This program is written using the C programming language.
 * The dictionary.h file is a header file used to
 * enforce the function definitions in the dictionary.c file.
 * It is also used to create the dictentry, initialize the
 * isEndOfWord boolean, and finally set the the *next array
 * to the 27 different characters being checked for 
 * in the search function => [a - z] & single quote ['].
 * This header file also creates the enum's returned
 * by the ResultType return type.
 *
 * @Author: Roman Mattia
 * @Date:   2020-01-30
 * @RedID:  820771491
 * @File:   dictionary.h
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>

#define Nchars 27

typedef struct dictentry {
    bool isEndOfWord;
    struct dictentry *next[Nchars];
} dictentry;

dictentry *getNewNode();

typedef enum {CORRECT, MISSPELLED, BADCHARS, ADDED, EXISTS} ResultType;

int wordToChildNodeIndex(char letter);

ResultType insert(struct dictentry *dictnode, const char *characters);

ResultType find(struct dictentry *dictnode, const char *targetword);
